# Probability flow dynamics for constrained stochastic nonlinear systems

(See also the repository [here](https://github.com/dimitra-maoutsa/DeterministicParticleFlowControl) )

Methods for computing constrained densities:

<img src="simulating_bridges.png" alt="simulating constrained densities by dimitra maoutsa" width="70%" height="70%">

Agreement between analytically derived and computed constrained density for terminal constraint:

<img src="figs/OU_reweight_bridge_pu.png" alt="analytical and numerical transient solution of a one dimensional constrained density by dimitra maoutsa" width="40%" height="40%">


Synchronisation control as path constrained control:


<img src="figs/N_kuramoto_synchronisation_uncoupled_N_500_M_40_g_0.250_Tsub_0.1000.png" alt="toy example of synchronisation of two interacting Kuramoto oscillators by dimitra maoutsa"  width="75%" height="75%">




